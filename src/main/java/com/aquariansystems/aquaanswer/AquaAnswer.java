package com.aquariansystems.aquaanswer;

import com.aquariansystems.aquaanswer.models.Account;
import com.aquariansystems.aquaanswer.models.AccountRole;
import com.aquariansystems.aquaanswer.models.Transaction;
import com.aquariansystems.aquaanswer.models.User;
import com.aquariansystems.aquaanswer.services.AccountRoleService;
import com.aquariansystems.aquaanswer.services.AccountService;
import com.aquariansystems.aquaanswer.services.TransactionService;
import com.aquariansystems.aquaanswer.services.UserService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.List;

public class AquaAnswer {
    private Connection conn;
    private UserService userService;
    private AccountService accountService;
    private AccountRoleService accountRoleService;
    private TransactionService transactionService;

    public AquaAnswer(String jdbcUri, int startHourOfDay, int endHourOfDay, int[] workDaysOfWeek) throws Exception {
        conn = DriverManager.getConnection(jdbcUri);
        userService = new UserService(conn);
        accountService = new AccountService(conn);
        accountRoleService = new AccountRoleService(conn);
        transactionService = new TransactionService(conn, startHourOfDay, endHourOfDay, workDaysOfWeek);
    }

    public void listUsers() throws Exception {
        List<User> users = userService.getAllUsers();
        for (User user : users) {
            System.out.println(user);
        }
    }

    public void listAccounts() throws Exception {
        List<Account> accounts = accountService.getAllAccounts();
        for (Account account : accounts) {
            System.out.println(account);
        }
    }

    public void listRoles() throws Exception {
        List<AccountRole> accountRoles = accountRoleService.getAllAccountRoles();
        for (AccountRole role : accountRoles) {
            System.out.println(role);
        }
    }

    public void listUserAccountRoles() throws Exception {
        Statement statement = conn.createStatement();
        String sql = "SELECT UAR.*, UA.USER_ID, UA.ACCOUNT_ID FROM APP.USER_ACCOUNT_ROLES AS UAR INNER JOIN APP.USER_ACCOUNTS UA on UA.ID = UAR.USER_ACCOUNT_ID";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            System.out.println("USER_ID: " + rs.getString("USER_ID") +
                    " ACCOUNT_ID: " + rs.getString("ACCOUNT_ID") +
                    " USER_ACCOUNT_ID: " + rs.getString("USER_ACCOUNT_ID") +
                    " ROLE_ID: " + rs.getString("ROLE_ID"));
        }
    }

    public void listTransactions() throws Exception {
        List<Transaction> transactions = transactionService.getTransactions();
        for (Transaction t : transactions) {
            System.out.println(t);
        }
    }

    public void listStatuses() throws Exception {
        Statement statement = conn.createStatement();
        String sql = "SELECT * FROM APP.TRANSACTION_STATUSES";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            System.out.println("ID: " + rs.getInt("ID") +
                    " STATUS_NAME: " + rs.getString("STATUS_NAME"));
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Usage: java AquaAnswer <path_to_derby_db>");
            System.exit(1);
        }
        String jdbcUri = "jdbc:derby:" + args[0];

        // These are usually from config
        int[] standardWorkWeek = {
                Calendar.MONDAY,
                Calendar.TUESDAY,
                Calendar.WEDNESDAY,
                Calendar.THURSDAY,
                Calendar.FRIDAY
        };
        int startHourOfDay = 8;
        int endHourOfDay = 16;

        AquaAnswer app = new AquaAnswer(jdbcUri, startHourOfDay, endHourOfDay, standardWorkWeek);
        System.out.println("Welcome to AquaAnswer");

        System.out.println("--------------------");
        System.out.println("Users:");
        app.listUsers();

        System.out.println("--------------------");
        System.out.println("Accounts:");
        app.listAccounts();

        System.out.println("--------------------");
        System.out.println("Roles:");
        app.listRoles();
        System.out.println("--------------------");
        System.out.println("User Account Roles:");
        app.listUserAccountRoles();


        System.out.println("--------------------");
        System.out.println("#1 Removing Mehrunes Dagon from A101");
        AccountRole originalRole = app.accountRoleService.getAccountRoleForUser("A101", "DC41DC7122BE41F9801C1D83D74D4159");
        app.accountService.removeUserFromAccount("DC41DC7122BE41F9801C1D83D74D4159", "A101");
        System.out.println("Users:");
        app.listUsers();
        System.out.println("User Account Roles:");
        app.listUserAccountRoles();

        System.out.println("--------------------");
        System.out.println("#2 Updating Mankar Camoran to Primary Custodian");
        app.accountRoleService.updateRoleForUser("A101", "81E84F647DC347BBB8B8B272766D0E1B", 1, 0);
        System.out.println("Users:");
        app.listUsers();

        System.out.println("--------------------");
        System.out.println("#3 Adding Adoring Fan to A101 as Alternate Custodian");
        app.accountService.addUserToAccount("813BDB47242D46D9B91BAD88982F183A", "A101", 1);
        System.out.println("Users:");
        app.listUsers();

        System.out.println("--------------------");
        System.out.println("Transactions: ");
        app.listTransactions();
        System.out.println("Statuses:");
        app.listStatuses();


        System.out.println("--------------------");
        System.out.println("#4 Updating transaction 2023-000016");
        app.transactionService.updateTransaction("2023-000016","A101",
                "81E84F647DC347BBB8B8B272766D0E1B", "DC41DC7122BE41F9801C1D83D74D4159", 2);
        System.out.println("Transactions: ");
        app.listTransactions();

    }


}
