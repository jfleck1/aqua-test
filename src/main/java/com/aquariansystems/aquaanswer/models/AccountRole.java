package com.aquariansystems.aquaanswer.models;

public class AccountRole {
    private int id;
    private String name;
    private int requiresTraining;
    private int canSignRecipient;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiresTraining() {
        return requiresTraining;
    }

    public void setRequiresTraining(int requiresTraining) {
        this.requiresTraining = requiresTraining;
    }

    public int getCanSignRecipient() {
        return canSignRecipient;
    }

    public void setCanSignRecipient(int canSignRecipient) {
        this.canSignRecipient = canSignRecipient;
    }

    @Override
    public String toString() {
        return "AccountRole{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", requiresTraining=" + requiresTraining +
                ", canSignRecipient=" + canSignRecipient +
                '}';
    }
}
