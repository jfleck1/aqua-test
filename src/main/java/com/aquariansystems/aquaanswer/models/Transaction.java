package com.aquariansystems.aquaanswer.models;

import java.time.Period;
import java.util.Calendar;
import java.util.Date;

public class Transaction {
    private String id;
    private String filingAccountId;
    private String filingNumber;
    private int filingYear;
    private int typeId;
    private int statusId;
    private Date creationDate;
    private Date completionDate;
    private String recipientId;
    private String witnessId;
    private String businessTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilingAccountId() {
        return filingAccountId;
    }

    public void setFilingAccountId(String filingAccountId) {
        this.filingAccountId = filingAccountId;
    }

    public String getFilingNumber() {
        return filingNumber;
    }

    public void setFilingNumber(String filingNumber) {
        this.filingNumber = filingNumber;
    }

    public int getFilingYear() {
        return filingYear;
    }

    public void setFilingYear(int filingYear) {
        this.filingYear = filingYear;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getWitnessId() {
        return witnessId;
    }

    public void setWitnessId(String witnessId) {
        this.witnessId = witnessId;
    }

    public String getBusinessTime() {
        return businessTime;
    }

    public void setBusinessTime(String businessTime) {
        this.businessTime = businessTime;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", filingAccountId='" + filingAccountId + '\'' +
                ", filingNumber='" + filingNumber + '\'' +
                ", filingYear=" + filingYear +
                ", typeId=" + typeId +
                ", statusId=" + statusId +
                ", creationDate=" + creationDate +
                ", completionDate=" + completionDate +
                ", recipientId='" + recipientId + '\'' +
                ", witnessId='" + witnessId + '\'' +
                ", businessTime=" + businessTime +
                '}';
    }
}
