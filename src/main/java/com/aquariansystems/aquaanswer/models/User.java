package com.aquariansystems.aquaanswer.models;

import java.util.List;
import java.util.Map;

public class User {
    private String id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Map<Account, AccountRole> accounts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Map<Account, AccountRole> getAccounts() {
        return accounts;
    }

    public void setAccounts(Map<Account, AccountRole> accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        String result = "User{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
        for (Account account : accounts.keySet()) {
            AccountRole role = accounts.get(account);
            result += "\n\t" + account + " " + role;
        }
        return result;
    }
}
