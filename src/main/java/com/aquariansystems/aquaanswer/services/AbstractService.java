package com.aquariansystems.aquaanswer.services;

import java.sql.Connection;
import java.util.Random;

public abstract class AbstractService {
    protected Connection conn;

    public AbstractService(Connection conn) {
        this.conn = conn;
    }

    protected String generateId(int length) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder builder = new StringBuilder();
        Random rnd = new Random();
        while (builder.length() < length) { // length of the random string.
            int index = (int) (rnd.nextFloat() * chars.length());
            builder.append(chars.charAt(index));
        }
        return builder.toString();
    }
}
