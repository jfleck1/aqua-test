package com.aquariansystems.aquaanswer.services;

import com.aquariansystems.aquaanswer.models.AccountRole;
import com.aquariansystems.aquaanswer.models.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountRoleService extends AbstractService {
    public AccountRoleService(Connection conn) {
        super(conn);
    }

    public List<AccountRole> getAllAccountRoles() throws Exception {
        List<AccountRole> result = new ArrayList<AccountRole>();

        Statement statement = conn.createStatement();
        String sql = "SELECT * FROM APP.ACCOUNT_ROLES";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            AccountRole role = new AccountRole();
            role.setId(rs.getInt("ID"));
            role.setName(rs.getString("NAME"));
            role.setRequiresTraining(rs.getInt("REQUIRES_TRAINING"));
            role.setCanSignRecipient(rs.getInt("CAN_SIGN_RECIPIENT"));

            result.add(role);
        }

        return result;
    }

    public AccountRole getAccountRoleForUser(String accountId, String userId) throws Exception {
        List<AccountRole> result = new ArrayList<AccountRole>();

        Statement statement = conn.createStatement();
        String sql = "SELECT AR.* FROM APP.ACCOUNT_ROLES AS AR " +
                "INNER JOIN APP.USER_ACCOUNT_ROLES AS UAR ON AR.ID=UAR.ROLE_ID " +
                "INNER JOIN APP.USER_ACCOUNTS AS UA ON UA.ID=UAR.USER_ACCOUNT_ID " +
                "WHERE UA.USER_ID = '" + userId + "' AND UA.ACCOUNT_ID = '" + accountId + "'";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            AccountRole role = new AccountRole();
            role.setId(rs.getInt("ID"));
            role.setName(rs.getString("NAME"));
            role.setRequiresTraining(rs.getInt("REQUIRES_TRAINING"));
            role.setCanSignRecipient(rs.getInt("CAN_SIGN_RECIPIENT"));

            result.add(role);
        }

        // TODO handle multiple results differently here
        return result.isEmpty() ? null : result.get(0);
    }

    public void updateRoleForUser(String accountId, String userId, int fromRoleId, int toRoleId) throws Exception {
        Statement statement = conn.createStatement();
        String sql = "UPDATE APP.USER_ACCOUNT_ROLES SET ROLE_ID=" + toRoleId + " WHERE ROLE_ID=" + fromRoleId +
                " AND USER_ACCOUNT_ID IN " +
                "(SELECT ID FROM APP.USER_ACCOUNTS WHERE USER_ID = '" + userId + "' AND ACCOUNT_ID = '" + accountId + "')";
        statement.executeUpdate(sql);
    }


}
