package com.aquariansystems.aquaanswer.services;

import com.aquariansystems.aquaanswer.models.Account;

import java.security.Provider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountService extends AbstractService {
    public AccountService(Connection conn) {
        super(conn);
    }

    public List<Account> getAllAccounts() throws SQLException {
        List<Account> result = new ArrayList<Account>();

        Statement statement = conn.createStatement();
        String sql = "SELECT * FROM APP.ACCOUNTS";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            Account account = new Account();
            account.setId(rs.getString("ID"));
            account.setName(rs.getString("NAME"));
            account.setParentAccountId(rs.getString("PARENT_ACCOUNT_ID"));
            account.setGeographicRegion(rs.getString("GEOGRAPHIC_REGION"));
            result.add(account);
        }

        return result;
    }

    public List<Account> getUserAccounts(String userId) throws Exception {
        List<Account> result = new ArrayList<Account>();

        Statement statement = conn.createStatement();
        String sql = "SELECT A.* FROM APP.ACCOUNTS AS A INNER JOIN APP.USER_ACCOUNTS AS UA ON A.ID = UA.ACCOUNT_ID " +
                "WHERE UA.USER_ID = '" + userId + "'";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            Account account = new Account();
            account.setId(rs.getString("ID"));
            account.setName(rs.getString("NAME"));
            account.setParentAccountId(rs.getString("PARENT_ACCOUNT_ID"));
            account.setGeographicRegion(rs.getString("GEOGRAPHIC_REGION"));
            result.add(account);
        }

        return result;
    }

    public void removeUserFromAccount(String userId, String accountId) throws Exception {
        Statement statement = conn.createStatement();

        String sql = "DELETE FROM APP.USER_ACCOUNT_ROLES WHERE USER_ACCOUNT_ID IN " +
                "(SELECT ID FROM APP.USER_ACCOUNTS WHERE USER_ID = '" + userId + "' AND ACCOUNT_ID = '" + accountId + "')";
        statement.executeUpdate(sql);

        sql = "DELETE FROM APP.USER_ACCOUNTS WHERE USER_ID = '" + userId + "' AND ACCOUNT_ID = '" +
                accountId + "'";

        statement.executeUpdate(sql);
    }

    public String addUserToAccount(String userId, String accountId, int roleId) throws Exception {
        String userAccountId = generateId(32);
        Statement statement = conn.createStatement();
        String sql = "INSERT INTO APP.USER_ACCOUNTS(ID, USER_ID, ACCOUNT_ID) VALUES('" +
                userAccountId + "', '" + userId + "', '" + accountId + "')";
        statement.executeUpdate(sql);

        sql = "INSERT INTO APP.USER_ACCOUNT_ROLES(USER_ACCOUNT_ID, ROLE_ID) VALUES('" + userAccountId + "', " + roleId + ")";
        statement.executeUpdate(sql);

        return userAccountId;
    }
}
