package com.aquariansystems.aquaanswer.services;

import com.aquariansystems.aquaanswer.models.Account;
import com.aquariansystems.aquaanswer.models.Transaction;
import com.aquariansystems.aquaanswer.utils.BusinessTime;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TransactionService extends AbstractService {
    private BusinessTime businessTime;

    public TransactionService(Connection conn, int startHourOfDay, int endHourOfDay, int[] workDaysOfWeek) {
        super(conn);
        businessTime = new BusinessTime(startHourOfDay, endHourOfDay, workDaysOfWeek);
    }

    public List<Transaction> getTransactions() throws Exception {
        List<Transaction> result = new ArrayList<Transaction>();

        Statement statement = conn.createStatement();
        String sql = "SELECT * FROM TRANSACTIONS";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            Transaction t = new Transaction();
            t.setId(rs.getString("ID"));
            t.setFilingAccountId(rs.getString("FILING_ACCOUNT_ID"));
            t.setFilingNumber(rs.getString("FILING_NUMBER"));
            t.setFilingYear(rs.getInt("FILING_YEAR"));
            t.setTypeId(rs.getInt("TYPE_ID"));
            t.setStatusId(rs.getInt("STATUS_ID"));
            t.setCreationDate(rs.getTimestamp("CREATION_DATE"));
            t.setCompletionDate(rs.getTimestamp("COMPLETION_DATE"));
            t.setRecipientId(rs.getString("RECIPIENT_ID"));
            t.setWitnessId(rs.getString("WITNESS_ID"));
            long businessTimeMs = businessTime.calculate(t.getCreationDate(), t.getCompletionDate());
            t.setBusinessTime(businessTime.format(businessTimeMs));
            result.add(t);
        }

        return result;
    }

    public void updateTransaction(String filingNumber, String accountId, String recipientId, String witnessId, int statusId)
            throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Statement statement = conn.createStatement();
        String sql = "UPDATE TRANSACTIONS SET " +
                "RECIPIENT_ID='" + recipientId + "', " +
                "WITNESS_ID='" + witnessId + "', " +
                "STATUS_ID=" + statusId + ", " +
                "COMPLETION_DATE=TIMESTAMP('" + dateFormat.format(new Date()) + "') " +
                "WHERE FILING_NUMBER='" + filingNumber + "' AND FILING_ACCOUNT_ID='" + accountId + "'";
        statement.executeUpdate(sql);
    }
}
