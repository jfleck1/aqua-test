package com.aquariansystems.aquaanswer.services;

import com.aquariansystems.aquaanswer.models.Account;
import com.aquariansystems.aquaanswer.models.AccountRole;
import com.aquariansystems.aquaanswer.models.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserService extends AbstractService {
    private AccountService accountService;
    private AccountRoleService accountRoleService;

    public UserService(Connection conn) {
        super(conn);
        this.accountService = new AccountService(conn);
        this.accountRoleService = new AccountRoleService(conn);
    }

    public List<User> getAllUsers() throws Exception {
        List<User> result = new ArrayList<User>();

        Statement statement = conn.createStatement();
        String sql = "SELECT * FROM APP.USERS";
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            User user = new User();
            user.setId(rs.getString("ID"));
            user.setLastName(rs.getString("LAST_NAME"));
            user.setFirstName(rs.getString("FIRST_NAME"));
            user.setMiddleName(rs.getString("MIDDLE_NAME"));

            List<Account> accounts = this.accountService.getUserAccounts(user.getId());
            Map<Account, AccountRole> accountRoles = new HashMap<Account, AccountRole>();
            for (Account account : accounts) {
                AccountRole role = this.accountRoleService.getAccountRoleForUser(account.getId(), user.getId());
                accountRoles.put(account, role);
            }
            user.setAccounts(accountRoles);

            result.add(user);
        }

        return result;
    }

    public void createUser(User user) throws Exception {
        Statement statement = conn.createStatement();
        String sql = "INSERT INTO APP.USERS (ID, LAST_NAME, FIRST_NAME, MIDDLE_NAME) VALUES ('"
                + user.getId() + "', '"
                + user.getLastName() + "', '"
                + user.getFirstName() + "', '"
                + user.getMiddleName() + "')";
        statement.execute(sql);
    }

    public void deleteUser(String id) throws Exception {
        Statement statement = conn.createStatement();
        String sql = "DELETE FROM APP.USERS WHERE ID = '" + id + "'";
        statement.execute(sql);
    }

}
