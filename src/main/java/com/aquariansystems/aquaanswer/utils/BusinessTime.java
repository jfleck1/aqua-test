package com.aquariansystems.aquaanswer.utils;

import org.apache.commons.lang.ArrayUtils;

import java.util.Calendar;
import java.util.Date;

public class BusinessTime {
    private int startHourOfDay;
    private int endHourOfDay;
    private int[] workDaysOfWeek;
    private final int MS_IN_AN_HOUR = 1000 * 60 * 60;
    private final int MS_IN_A_WORKDAY;

    public BusinessTime(int startHourOfDay, int endHourOfDay, int[] workDaysOfWeek) {
        this.startHourOfDay = startHourOfDay;
        this.endHourOfDay = endHourOfDay;
        this.workDaysOfWeek = workDaysOfWeek;
        this.MS_IN_A_WORKDAY = (endHourOfDay - startHourOfDay) * MS_IN_AN_HOUR;
    }

    public long calculate(Date start, Date end) throws BusinessTimeException {
        long totalMs = 0;

        if (start == null || end == null) {
            return 0;
        }

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(start);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(end);

        // Are these two dates on the same day?
        if (startCal.get(Calendar.YEAR) == endCal.get(Calendar.YEAR) &&
                startCal.get(Calendar.DAY_OF_YEAR) == endCal.get(Calendar.DAY_OF_YEAR)) {
            // Just use them to get the ms
            totalMs = getSameWorkdayMs(start, end);
        } else {
            // Count the first partial day, then loop through each day after that, count the work ms, until the end date
            totalMs = getSameWorkdayMs(start, false); // partial day

            Calendar currentCal = Calendar.getInstance();
            currentCal.setTime(start);

            // Set current calendar to SOB the following day
            currentCal.set(Calendar.HOUR_OF_DAY, startHourOfDay);
            currentCal.add(Calendar.DAY_OF_MONTH, 1);
            Date current = currentCal.getTime();

            // The current is now the next day SOB, continue until current is before end
            while (current.before(end) || current.equals(end)) {
                Date cob = getEndOfWorkday(current);

                if (end.before(cob) || end.equals(cob)) {
                    // Partial end day
                    long endDayMs = getSameWorkdayMs(end, true);
                    totalMs += endDayMs;
                    break;
                } else {
                    // Count full day
                    totalMs += MS_IN_A_WORKDAY;
                }

                currentCal.add(Calendar.DAY_OF_MONTH, 1);
                current = currentCal.getTime();
            }
        }

        return totalMs;
    }

    public long getSameWorkdayMs(Date current, boolean sinceStart) throws BusinessTimeException {
        if (sinceStart) {
            return getSameWorkdayMs(getStartOfWorkday(current), current);
        } else {
            return getSameWorkdayMs(current, getEndOfWorkday(current));
        }
    }

    public long getSameWorkdayMs(Date from, Date to) throws BusinessTimeException {
        long result = 0;

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(from);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(to);

        // Verify the dates are on the same day
        if (startCal.get(Calendar.YEAR) != endCal.get(Calendar.YEAR) ||
                startCal.get(Calendar.DAY_OF_YEAR) != endCal.get(Calendar.DAY_OF_YEAR)) {
            throw new BusinessTimeException("Dates are not on the same day");
        }

        if (isWorkDay(from) && isWorkDay(to)) {
            Date sob = getStartOfWorkday(from);
            Date cob = getEndOfWorkday(from);

            if (from.before(sob)) {
                from = sob;
            }

            if (to.after(cob)) {
                to = cob;
            }

            result =  to.getTime() - from.getTime();
        }

        return result;
    }

    public Date getStartOfWorkday(Date date) {
        return getHourOfDay(date, startHourOfDay);
    }

    public Date getEndOfWorkday(Date date) {
        return getHourOfDay(date, endHourOfDay);
    }

    public Date getHourOfDay(Date date, int hourOfDay) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);

        // Zeroize time less than an hour
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public boolean isWorkDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        return ArrayUtils.contains(workDaysOfWeek, dayOfWeek);
    }

    public String format(long milliseconds) {
        long workdays = milliseconds / MS_IN_A_WORKDAY;
        double remainder = milliseconds % MS_IN_A_WORKDAY;
        long hours = (long) remainder / MS_IN_AN_HOUR;
        remainder = remainder % MS_IN_AN_HOUR;
        long minutes = (long) remainder / (1000 * 60);
        remainder = remainder % (1000 * 60);
        long seconds = (long) remainder / (1000);
        remainder = remainder % (1000);
        long ms = (long) remainder;

        return "workdays: " + workdays + ", hours: " + hours + ", minutes: " + minutes + ", seconds: " + seconds +
                ", ms: " + ms;

    }

}
