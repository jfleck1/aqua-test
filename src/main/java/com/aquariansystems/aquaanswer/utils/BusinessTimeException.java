package com.aquariansystems.aquaanswer.utils;

public class BusinessTimeException extends Exception {
    public BusinessTimeException() {
    }

    public BusinessTimeException(String message) {
        super(message);
    }

    public BusinessTimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessTimeException(Throwable cause) {
        super(cause);
    }

    public BusinessTimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
