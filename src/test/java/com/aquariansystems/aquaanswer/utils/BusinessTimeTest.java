package com.aquariansystems.aquaanswer.utils;

import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Date;

public class BusinessTimeTest {
    final int[] STANDARD_WORK_WEEK = {
            Calendar.MONDAY,
            Calendar.TUESDAY,
            Calendar.WEDNESDAY,
            Calendar.THURSDAY,
            Calendar.FRIDAY
    };
    final int START_HOUR_OF_DAY = 8;
    final int END_HOUR_OF_DAY = 16;

    SimpleDateFormat iso8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    final long MS_IN_AN_HOUR = 1000 * 60 * 60;
    final long MS_IN_A_WORKDAY = (END_HOUR_OF_DAY - START_HOUR_OF_DAY) * MS_IN_AN_HOUR;

    @Test
    public void testIsWorkDay() throws Exception {
        BusinessTime bt = new BusinessTime(START_HOUR_OF_DAY, END_HOUR_OF_DAY, STANDARD_WORK_WEEK);

        assert bt.isWorkDay(iso8601.parse("2024-07-03T14:22:23.230-0400"));
        assert !bt.isWorkDay(iso8601.parse("2024-07-06T14:22:23.230-0400"));
    }

    @Test
    public void testGetWorkdayMs() throws Exception {
        BusinessTime bt = new BusinessTime(START_HOUR_OF_DAY, END_HOUR_OF_DAY, STANDARD_WORK_WEEK);

        // Workday date, to the close of business
        Date date = iso8601.parse("2024-07-03T15:00:00.000-0400");
        long workdayMs = bt.getSameWorkdayMs(date, false);

        assert workdayMs == 3600000;

        // Workday date, from the start of business
        date = iso8601.parse("2024-07-03T09:00:00.509-0400");
        workdayMs = bt.getSameWorkdayMs(date, true);
        assert workdayMs == 3600509;

        // Date is on the weekend
        date = iso8601.parse("2024-07-06T09:00:00.509-0400");
        workdayMs = bt.getSameWorkdayMs(date, true);
        assert workdayMs == 0;

        // Date is at night
        date = iso8601.parse("2024-07-03T23:00:00.509-0400");
        workdayMs = bt.getSameWorkdayMs(date, true);
        assert workdayMs == MS_IN_A_WORKDAY;
    }

    @Test
    public void testCalculate() throws Exception {
        BusinessTime bt = new BusinessTime(START_HOUR_OF_DAY, END_HOUR_OF_DAY, STANDARD_WORK_WEEK);

        Date start;
        Date end;
        long totalMs;

        // Times within workdays, spanning multiple days
        start = iso8601.parse("2024-06-24T09:00:00.000-0400");
        end = iso8601.parse("2024-06-28T15:00:00.000-0400");
        totalMs = bt.calculate(start, end);
        long expected = MS_IN_AN_HOUR * 7 + MS_IN_A_WORKDAY * 3 + MS_IN_AN_HOUR * 7;
        assert totalMs == expected;

        // Times outside workdays, spanning multiple days
        start = iso8601.parse("2024-06-24T02:00:00.000-0400");
        end = iso8601.parse("2024-06-28T23:00:00.000-0400");
        totalMs = bt.calculate(start, end);
        assert totalMs == MS_IN_A_WORKDAY * 5;

        // Times within workdays, spanning 1 day
        start = iso8601.parse("2024-06-24T09:00:00.000-0400");
        end = iso8601.parse("2024-06-24T15:00:00.000-0400");
        totalMs = bt.calculate(start, end);
        assert totalMs == MS_IN_AN_HOUR * 6;

        // Times outside workdays, spanning 1 day
        start = iso8601.parse("2024-06-24T02:00:00.000-0400");
        end = iso8601.parse("2024-06-24T23:00:00.000-0400");
        totalMs = bt.calculate(start, end);
        assert totalMs == MS_IN_A_WORKDAY;
    }
}
